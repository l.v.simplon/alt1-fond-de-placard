import { Injectable } from '../sketchyframework/dependencyinjector';
import { IngredientsRepository } from '../repository/ingredients';

@Injectable()
export class IngredientService {
    constructor(private ingredientsRepo: IngredientsRepository) {
    }

    search(searchTerm: string) {
        return this.ingredientsRepo.find(searchTerm);
    }

    getIngredient(name: string) {
        return this.ingredientsRepo.getOne(name);
    }

    addIngredient(name: string) {
        return this.ingredientsRepo.insertOne(name);
    }
};
