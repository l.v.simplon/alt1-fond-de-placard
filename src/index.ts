import { BasicRouter } from './sketchyframework/mybadexpress';
import { recursiveInstantiate, get, register } from './sketchyframework/dependencyinjector';

import { DBConnection } from './dbconnection';

import {IngredientsController} from './controllers/ingredients';
import {RecipesController} from './controllers/recipes';


const dbConn = new DBConnection({
    path: ':memory:',
})

register(DBConnection, dbConn);

recursiveInstantiate(IngredientsController);
recursiveInstantiate(RecipesController);


const server = get(BasicRouter);
server.listen(8080);
