import { parseJSONBody, BasicRouter } from '../sketchyframework/mybadexpress';
import { RecipesService } from '../services/recipes';
import { Injectable } from '../sketchyframework/dependencyinjector';

@Injectable()
export class RecipesController {
    constructor(
        private server: BasicRouter,
        private recipesService: RecipesService
    ) {
        this.setupRoutes()
    }

    setupRoutes() {
        this.server.post('/api/recipes/add', async ({req, res, ctx}) => {
            const body = await parseJSONBody(req);

            let opRes;
            try {
                const {name, ingredients, category, picture} = body;
                opRes = await this.recipesService.addRecipe(name, ingredients, category, picture);
            } catch(e) {
                //not sure where this should be handled and/or what to reply to client
                //I suppose I need to translate sqlite errors into "generic" error that the service understands?
                res.writeHead(500);
                res.end();
                return;
            }

            res.writeHead(200);
            res.end();
        })

        this.server.post('/api/recipes/search/byname', async ({req, res, json}) => {
            const body = await parseJSONBody(req);

            const searchRes = await this.recipesService.searchByName(body.searchTerm);

            json(searchRes);
        })
        this.server.post('/api/recipes/search/byingredients', async ({req, res, json}) => {
            const body = await parseJSONBody(req);

            const searchRes = await this.recipesService.searchByIngredients(body.ingredients);

            json(searchRes);
        })
    }

    //@route('/todo')
    search() {
    }
}
