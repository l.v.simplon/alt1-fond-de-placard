const http = require('http');

/*class Handler {
    constructor(route, method, handler) {
        this.route = route;
        this.method = method;
        this.handler = handler;
    }
}*/

export class BasicRouter {
    server;
    port;
    listening = false;
    serverOptions = {};
    routeMap = new Map();

    constructor() {
        this.server = http.createServer(this.serverOptions, this.handleRequest.bind(this));
    }

    get(route, handler) {
        this.addRoute("GET", route, handler);
    }

    post(route, handler) {
        this.addRoute("POST", route, handler);
    }

    addRoute(method, route, handler) {
        let routesForMethod = this.getRoutesForMethod(method);

        if (!routesForMethod.has(route)) {
            routesForMethod.set(route, []);
        }

        routesForMethod.get(route).push(handler);
    }

    getRoutesForMethod(method) {
        const routesForMethod = this.routeMap.get(method)

        if (routesForMethod)
            return routesForMethod;

        const map = new Map();
        this.routeMap.set(method, map);
        return map;
    }

    handleRequest(request, response) {
        const {method, url, headers} = request;
        console.info("handling request", url)

        const handlers = this.findMatchingRoute(method, url);

        if (!handlers.length) {
            response.writeHead(404);
            response.end(`no route for ${url}`);
            return;
        }

        let i = 0;
        const ctx = {
            request,
            response,
            req: request,
            res: response,
            json: sendJSON.bind(null, response),
        };
        const next = async () => {
            if (!handlers[i]) {
                return ;
            }

            console.info("executing", handlers[i].toString())
            await handlers[i]({...ctx, next, ctx});
            i++;
        }
        //ctx.next = next;
        //ctx.ctx = ctx;

        next();
    }

    findMatchingRoute(method, path) {
        //TODO: impossible to reply to OPTIONS with this
        let routesForMethod = this.getRoutesForMethod(method);
        //todo: matching patterns

        const matches = [...routesForMethod.keys()].filter((route) => {
            return route == path;
        })

        //todo redo this
        return matches.flatMap((route) => routesForMethod.get(route));
    }

    listen(port) {
        if (this.listening) {
            throw new Error("Server already runnign");
        }

        this.port = port;
        this.listening = true;
        this.server.listen(this.port); //TODO this is async
    }
}

//====

const parseBody = async (req, parser) => {
    return new Promise((resolve, reject) => {
        let data = '';
        
        req.on('data', chunk => data += chunk)
        req.on('end', () => {
            resolve(parser(data));
        })
    })
}

const sendJSON = (res, data, statusCode = 200) => {
    res.writeHead(statusCode, {
        'Content-Type': 'application/json'
    });

    res.write(JSON.stringify(data));
    res.end();
}

//TODO: check content-type header?
export const parseJSONBody = async (req): Promise<any> => {
    return parseBody(req, JSON.parse);
}
