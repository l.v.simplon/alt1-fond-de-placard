import { Injectable } from './sketchyframework/dependencyinjector';
import * as sqlite3 from 'sqlite3';

@Injectable()
export class DBConnection {
    db: sqlite3.Database;

    constructor(dbConfig) {
        this.db = new (sqlite3.verbose()).Database(dbConfig.path);
    }

    getDb() {
        return this.db;
    }
}
