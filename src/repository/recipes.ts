import { Injectable } from '../sketchyframework/dependencyinjector';
import BaseRepository from "./BaseRepository";
import { DBConnection } from "../dbconnection";
import { RecipeEntity } from "../entities/recipes";
import { IngredientsRepository } from "./ingredients";
import { IngredientEntity } from '../entities/ingredients';

@Injectable()
export class RecipesRepository extends BaseRepository<RecipeEntity> {
    protected tableName = "recipes";

    constructor(
      protected dbConnection: DBConnection,
      private ingredientsRepo: IngredientsRepository, //not sure if I should use the repo or the service
    ) {
        super();
        this.createTable();
    }

    //TODO: generate this via the Entity's fields
    createTable() {
        this.dbConnection.getDb().run(`
            CREATE TABLE IF NOT EXISTS ${this.tableName} (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name STRING UNIQUE,
                category STRING,
                picture STRING,
                score INTEGER
            )`);

            this.dbConnection.getDb().run(`
                CREATE TABLE IF NOT EXISTS ${"recipes_to_ingredients" /*damn it*/} (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    ingredientId INTEGER NOT NULL,
                    recipeId INTEGER NOT NULL,
                    FOREIGN KEY (ingredientId) REFERENCES ingredients (id),
                    FOREIGN KEY (recipeId) REFERENCES recipes (id)
                )`);
    }

    async insertOne(name: string, ingredients: string[], category, picture) {
        let recipeE;
        
        try {
            recipeE = await new Promise<RecipeEntity>((resolve, reject) => {
                const stmt = this.dbConnection.getDb().prepare(`INSERT INTO ${this.tableName} (name, category, picture, score) VALUES (?, ?, ?, ?)`);
                stmt.run([name, category, picture], (err, res) => {
                    const lastInsertedID = stmt['lastID'];
                    return err ? reject(err) : resolve({name, id: lastInsertedID, ingredients: []});
                })
            })
        } catch(e) {
            if (e.errno === 19 && e.code === 'SQLITE_CONSTRAINT')
                throw new Error("A recipe with this name already exists");
            
            throw e;
        }

        const jointureInsertionPromises = ingredients.map(ingredient => new Promise(async (resolve, reject) => {
            const ingredientE = await this.ingredientsRepo.getOrAdd(ingredient);

            console.log("joining", ingredientE, recipeE);

            const stmt = this.dbConnection.getDb().prepare(`INSERT INTO ${"recipes_to_ingredients"} (ingredientId, recipeId) VALUES (?, ?)`);
            stmt.run([ingredientE.id, recipeE.id], (err, res) => {
                const lastInsertedID = stmt['lastID'];
                return err ? reject(err) : resolve(lastInsertedID);
            })
        }));

        await Promise.all(jointureInsertionPromises);
    }

    async getOne(name) {
        const recipeE = await this.getOneBy('name', name);

        if (!recipeE)
            return;

        const joint = await new Promise<any[]>((resolve, reject) => {
            this.dbConnection.getDb().all(
                `SELECT * FROM ${"recipes_to_ingredients"} where recipeId = ?`,
                [recipeE.id],
                (err, row) => err ? reject(err) : resolve(row)
            )
        })

        const ingredients = await Promise.all(joint.map((join) => this.ingredientsRepo.getOneById(join.ingredientId)));
        recipeE.ingredients = ingredients as IngredientEntity[];

        return recipeE;
    }

    findByName(str): Promise<RecipeEntity[]> {
        str = '%' + str + '%';

        return new Promise((resolve, reject) => {
            this.dbConnection.getDb().all(
                `SELECT * FROM ${this.tableName} WHERE name LIKE ?`,
                [str],
                (err, row) => err ? reject(err) : resolve(row)
            )
        })
    }

    async findByIngredients(ingredients: string[]): Promise<string[]> {
        const ingredientsE = (await Promise.all(ingredients.map(name => this.ingredientsRepo.getOne(name)))).filter(e => e !== undefined) as IngredientEntity[];

        const ingredientIds = ingredientsE.map(i => i.id);
        return new Promise((resolve, reject) => {
            const stmt = this.dbConnection.getDb().prepare(
                `SELECT name FROM ${this.tableName} as recipes
                    LEFT JOIN ${"recipes_to_ingredients"} as link
                    ON recipes.id = link.recipeId
                    WHERE link.ingredientId IN (${ingredientIds.map(e => '?').join(', ')})
                    GROUP BY recipes.id HAVING COUNT(*)= ?;
                `)
                // https://github.com/mapbox/node-sqlite3/issues/762

            stmt.all(
                [...ingredientIds, ingredientIds.length],
                (err, rows) => err ? reject(err) : resolve(rows.map(row => row.name))
            );
        });
    }
}
