import { Injectable } from '../sketchyframework/dependencyinjector';
import BaseRepository from "./BaseRepository";
import { DBConnection } from "../dbconnection";
import { IngredientEntity } from "../entities/ingredients";

@Injectable()
export class IngredientsRepository extends BaseRepository<IngredientEntity> {
    protected tableName = "ingredients";

    constructor(protected dbConnection: DBConnection) {
        super();
        this.createTable();
    }

    //TODO: generate this via the Entity's fields
    createTable() {
        this.dbConnection.getDb().run(`
            CREATE TABLE IF NOT EXISTS ${this.tableName} (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name STRING UNIQUE
            )`);
    }

    async insertOne(name: string) {
        try {
            return await new Promise((resolve, reject) => {
                const stmt = this.dbConnection.getDb().prepare(`INSERT INTO ${this.tableName} (name) VALUES (?)`);
                stmt.run(name, (err, res) => {
                    const lastInsertedID = stmt['lastID'];
                    return err ? reject(err) : resolve({name, id: lastInsertedID});
                })
            })
        } catch(e) {
            if (e.errno === 19 && e.code === "SQLITE_CONSTRAINT")
                return this.getOne(name); //TODO entity

            throw e;
        }
    }

    getOrAdd(name) {
        //turns out the logic is already into insertOne right now
        return <Promise<IngredientEntity>>this.insertOne(name);
    }


    getOne(name) {
        return this.getOneBy('name', name);
    }

    find(str): Promise<IngredientEntity[]> {
        str = '%' + str + '%';

        return new Promise((resolve, reject) => {
            this.dbConnection.getDb().all(
                `SELECT * FROM ${this.tableName} WHERE name LIKE ?`,
                [str],
                (err, row) => err ? reject(err) : resolve(row)
            )
        })
    }
}
