import {DBConnection} from '../dbconnection';

export default class BaseRepository<T> {
    protected dbConnection: DBConnection;
    protected tableName: string;

    constructor() {
    }

    getOneById = (id: number) => {
        return this.getOneBy("id", id);
    }

    getOneBy = (fieldName: string, value: any): Promise<T | undefined> => {
        return new Promise((resolve, reject) => {
            this.dbConnection.getDb().get(
                `SELECT * FROM ${this.tableName} where ${fieldName} = ? LIMIT 1`,
                [value],
                (err, row) => err ? reject(err) : resolve(row)
            )
        })
    }

    getAll = (): Promise<T[]> => {
        return new Promise((resolve, reject) => {
            this.dbConnection.getDb().all(
                `SELECT * FROM ${this.tableName}`,
                [],
                (err, row) => err ? reject(err) : resolve(row)
            )
        })
    }
}
