echo "adding an ingredient"
curl localhost:8080/api/ingredient/add --header "Content-Type:application/json" --data '{"name":"xyz"}' -v

echo
echo "=============="
echo "adding a recipe + ingredients"
curl localhost:8080/api/recipes/add --header "Content-Type: application/json" --data '{"name": "test2", "ingredients": ["xyz", "apple", "potato", "tabacco", "old shoe", "bath bomb salt"], "category": "breakfast", "picture": "https://thispersondoesnotexist.com/"}' -v
curl localhost:8080/api/recipes/add --header "Content-Type: application/json" --data '{"name": "test", "ingredients": ["raspberry", "apple"], "category": "breakfast", "picture": "https://thispersondoesnotexist.com/"}' -v

echo
echo "=============="
echo "searching a recipe by name"
curl localhost:8080/api/recipes/search/byname --header "Content-Type: application/json" --data '{"searchTerm": "te"}'

echo
echo "=============="
echo "searching a recipe by ingredients"
curl localhost:8080/api/recipes/search/byingredients --header "Content-Type: application/json" --data '{"ingredients": ["apple"]}'

echo
curl localhost:8080/api/recipes/search/byingredients --header "Content-Type: application/json" --data '{"ingredients": ["apple", "raspberry"]}'

echo
curl localhost:8080/api/recipes/search/byingredients --header "Content-Type: application/json" --data '{"ingredients": ["apple", "xyz"]}'

echo
curl localhost:8080/api/recipes/search/byingredients --header "Content-Type: application/json" --data '{"ingredients": ["pear"]}'


echo
echo "=============="
echo "searching an ingredient"
curl localhost:8080/api/ingredient/search --header "Content-Type: application/json" --data '{"searchTerm":"ba"}'
